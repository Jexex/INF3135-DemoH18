#include "couleur.h"
#include <CUnit/Basic.h>

// int main(int argc, char const *argv[])
// {
//   // Creation d'image
  
//   Couleur couleur1010 = image_obtenirPixel(&imgNoir, 10, 10);

//   printf("%s\n", "TEST1");
//   printf("Couleur du pixel (10,10) : r:%d, g:%d, b:%d, a:%d\n",
//     couleur1010.r, couleur1010.g, couleur1010.b, couleur1010.a);

//   image_modifierPixel(&imgNoir, 10, 10, 20, 30, 40, 0);
//   couleur1010 = image_obtenirPixel(&imgNoir, 10, 10);

//   printf("%s\n", "TEST2");
//   printf("Couleur du pixel (10,10) : r:%d, g:%d, b:%d, a:%d\n",
//     couleur1010.r, couleur1010.g, couleur1010.b, couleur1010.a);

//   return 0;
// }

int init_suite1(void)
{
   return 0;
}

/* The suite cleanup function.
 * Closes the temporary file used by the tests.
 * Returns zero on success, non-zero otherwise.
 */
int clean_suite1(void)
{
   return 0;
}

void testImage_noire(void){
  Image imgNoir = image_noire(100,100);
  CU_ASSERT(imgNoir->largeur == 100);
  CU_ASSERT(imgNoir->hauteur == 100);
  // CU_ASSERT(imgNoir->pixels == 100);
}

void testImage_obtenirPixel(void){
  Image imgNoir = image_noire(100,100);
  Couleur couleur = image_obtenirPixel(&imgNoir, 10, 10);
  CU_ASSERT(couleur.r == 0);
  CU_ASSERT(couleur.g == 0);
  CU_ASSERT(couleur.b == 0);
  CU_ASSERT(couleur.a == 1);
}

int main(int argc, char const *argv[])
{
   CU_pSuite pSuite = NULL;

   /* initialize the CUnit test registry */
   if (CUE_SUCCESS != CU_initialize_registry())
      return CU_get_error();

   /* add a suite to the registry */
   pSuite = CU_add_suite("Suite_1", init_suite1, clean_suite1);
   if (NULL == pSuite) {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* add the tests to the suite */
   /* NOTE - ORDER IS IMPORTANT - MUST TEST fread() AFTER fprintf() */
   if ((NULL == CU_add_test(pSuite, "Test de creation IMAGE", testImage_noire)) ||
      (NULL == CU_add_test(pSuite, "Test obtenir pixels", testImage_obtenirPixel)))
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* Run all tests using the CUnit Basic interface */
   CU_basic_set_mode(CU_BRM_VERBOSE);
   CU_basic_run_tests();
   CU_cleanup_registry();
   return CU_get_error();
}