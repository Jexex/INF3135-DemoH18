#include "couleur.h"

Image image_noire(unsigned int largeur, unsigned int hauteur){
  Image image = (Image) malloc(sizeof(Image));
  image->largeur = largeur;
  image->hauteur = hauteur;
  image->pixels = malloc(largeur*hauteur*sizeof(Couleur*));
  int i;
  for (i = 0; i < largeur*hauteur; ++i){
    image->pixels[i] = (Couleur*)malloc(sizeof(Couleur));
    image->pixels[i]->r = 0;
    image->pixels[i]->g = 0;
    image->pixels[i]->b = 0;
    image->pixels[i]->a = 1;
  }
  return image;
}

struct Couleur image_obtenirPixel(const Image *image,
                                  unsigned int i,
                                  unsigned int j){
  unsigned int pixel = j * (*image)->hauteur + i;
  return *((*image)->pixels[pixel]);
}

void image_modifierPixel(Image *image,
                         unsigned int i,
                         unsigned int j,
                         unsigned int r,
                         unsigned int g,
                         unsigned int b,
                         unsigned int a){
  unsigned int pixel = j * (*image)->hauteur + i;
  Couleur* couleur = (*image)->pixels[pixel];
  couleur->r = r;
  couleur->g = g;
  couleur->b = b;
  couleur->a = a;
}

bool image_sontEgales(const Image image1,
                      const Image image2){
  return true;
}

void image_reflechirHorizontalement(Image image){

}

void image_rotationHoraire(Image image){

}

Image image_melangerImages(const Image image1,
                           const Image image2,
                           float poids){
  return NULL;
}