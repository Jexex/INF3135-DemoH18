#include "stdio.h"
#include "math.h"

enum TypeNombre {
  INT, FLOAT, DOUBLE
};

typedef struct {           // Un nombre
  enum TypeNombre type;  // Le type de nombre
  union {
    int i;
    float f;
    double d;
  } valeur;              // La valeur
} Nombre;

Nombre max(Nombre a, Nombre b);

int main(int argc, char const *argv[])
{
  Nombre a;
  Nombre b;

  a.type = INT;
  a.valeur.i = 20;

  b.type = DOUBLE;
  b.valeur.d = 15.6;

  Nombre plusGrand = max(a,b);

  return 0;
}

Nombre max(Nombre a, Nombre b){
  double aValeur;
  double bValeur;
  switch(a.type){
    case INT:
      aValeur = (double) a.valeur.i;
      break;
    case FLOAT:
      aValeur = (double) a.valeur.f;
      break;
    case DOUBLE:
      aValeur = (double) a.valeur.d;
      break;
    default:
    break;
  }
  switch(b.type){
    case INT:
      bValeur = (double) b.valeur.i;
      break;
    case FLOAT:
      bValeur = (double) b.valeur.f;
      break;
    case DOUBLE:
      bValeur = (double) b.valeur.d;
      break;
    default:
    break;
  }
  printf("a => %f b => %f\n", aValeur, bValeur);
  if(aValeur > bValeur) return a;
  return b;
}