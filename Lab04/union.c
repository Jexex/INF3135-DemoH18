#include "stdio.h"
#include "math.h"

struct Couple {
  double elem1;
  double elem2;
};

struct Solution {
  unsigned int nbSolutions;    // Le nombre de solutions de l'équation
  union {
    double solution1;        // La solution unique quand delta = 0
    struct Couple solution2; // Les deux solutions quand delta > 0
  } solutions;
};

struct Solution resoudreEquation(double a, double b, double c);
void afficherSolution(const struct Solution *solution);

int main(int argc, char const *argv[])
{
  // 2x^2 + 5x -3 = 0
  struct Solution test1 = resoudreEquation(2.0, 5.0, -3.0);
  afficherSolution(&test1);

  // 9x^2 + 12x + 4 = 0
  struct Solution test2 = resoudreEquation(9.0, 12.0, 4.0);
  afficherSolution(&test2);

  // x^2 + 3x + 3 = 0
  struct Solution test3 = resoudreEquation(1.0, 3.0, 3.0);
  afficherSolution(&test3);

  return 0;
}

struct Solution resoudreEquation(double a, double b, double c){
  double delta = pow(b, 2) - 4*a*c;
  struct Solution solution;
  if(delta > 0){
    double sol1 = (-b-sqrt(delta))/(2*a);
    double sol2 = (-b+sqrt(delta))/(2*a);
    struct Couple couple = {sol1, sol2};
    solution.nbSolutions = 2;
    solution.solutions.solution2 = couple;
  }else if(delta == 0){
    solution.nbSolutions = 1;
    double sol = -b/(2*a);
    solution.solutions.solution1 = sol;
  }else{
    solution.nbSolutions = 0;
  }
  return solution;
}

void afficherSolution(const struct Solution *solution){
  if(solution->nbSolutions == 2){
    printf("L'equation admet deux solutions : %.2f et %.2f\n", solution->solutions.solution2.elem1, solution->solutions.solution2.elem2);
  }else if(solution->nbSolutions == 1){
    printf("L'equation admet une solution : %.2f\n", solution->solutions.solution1);
  }else{
    printf("%s\n", "L'equation n'admet aucune solution");
  }
}