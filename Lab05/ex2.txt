Commande 1 : man printf | head -n 20 > man-printf.txt
Commande 2 : curl https://gitlab.com/ablondin/inf3135-exercices/raw/master/exemples/array.c | grep -E '//.*'
Commande 3 : git log | grep -E 'Author:*' | sort | uniq
Commande 4 : git log | grep -E '^Author:*' | sed 's/^.*: //' | sed 's/ <.*$//' | sort | uniq
