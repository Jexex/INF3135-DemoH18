#include "stdio.h"
#include "string.h"
#include "stdbool.h"

bool estPalindrome(const char *s);
bool estPhrasePalindrome(const char *s);

int main(int argc, char const *argv[])
{
  char* test[] = {"radar", "ici", "laval", "!RessasseR!", "", "U", "Lui", "Ici", "laval.", "Esope reste et se repose."};
  int len = 10;

  // Test estPalindrome
  printf("%s\n", "\nTest de estPalindrome\n--------------");
  int i;
  for (i = 0; i < len; ++i)
  {
    if(estPalindrome(test[i])){
      printf("%s est un palindrome\n", test[i]);
    }else{
      printf("%s n'est pas un palindrome\n", test[i]);
    }
  }

  // Test estPhrasePalindrome
  printf("%s\n", "\nTest de estPhrasePalindrome\n--------------");
  for (i = 0; i < len; ++i)
  {
    if(estPhrasePalindrome(test[i])){
      printf("%s est un palindrome\n", test[i]);
    }else{
      printf("%s n'est pas un palindrome\n", test[i]);
    }
  }
  return 0;
}

bool estPalindrome(const char *s){
  int longueur = strlen(s);
  int i;
  for (i = 0; i < longueur/2; ++i)
  {
    if(s[i] != s[longueur-i-1]){
      return false;
    }
  }
  return true;
}














bool estPhrasePalindrome(const char *s){
  int longueur = strlen(s);
  int i;
  int j = longueur-1;
  for (i = 0; i < longueur/2; ++i, --j)
  {
    while(!isalpha(s[i])){
      i++;
    }
    while(!isalpha(s[j])){
      j--;
    }
    if(tolower(s[i]) != tolower(s[j])){
      return false;
    }
  }
  return true;
}