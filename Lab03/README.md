# Solution laboratoire 3 | 26 septembre 2017

[Énoncé du laboratoire](https://gitlab.com/ablondin/inf3135-exercices/blob/master/seance3.md)

## Fichiers dans la solution
* [lab3.c](https://gitlab.com/rmTheZ/demo-inf3135/tree/master/Lab03/lab03.c) Contient la solution des 4 fonctions à implémenter + la gestion des arguments
* [makefile](https://gitlab.com/rmTheZ/demo-inf3135/tree/master/Lab03/makefile) Contient un makefile minimum

## Utilisation
```shell
$ make clean
$ make
$ ./lab2 TonNom
```
