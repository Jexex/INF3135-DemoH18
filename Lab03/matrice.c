#include <stdio.h>

void afficherMatrice(int matrice[3][3]);
void additionMatrice(int matrice1[3][3], int matrice2[3][3], int matriceTotal[3][3]);

int main(int argc, char const *argv[]) {
  int matrice1[3][3] = {{1,2,3},{4,5,6},{7,8,9}};
  int matrice2[3][3] = {{1,1,1},{1,1,1},{1,1,1}};
  int matriceTotal[3][3];

  additionMatrice(matrice1, matrice2, matriceTotal);

  afficherMatrice(matrice1);
  printf("%s\n", "+");
  afficherMatrice(matrice2);
  printf("%s\n", "=");
  afficherMatrice(matriceTotal);

  return 0;
}

void afficherMatrice(int matrice[3][3]){
  int i;
  int j;
  for (i = 0; i < 3; i++) {
    for (j = 0; j < 3; j++) {
      printf("%d ", matrice[i][j]);
    }
    printf("\n");
  }
}

void additionMatrice(int matrice1[3][3], int matrice2[3][3], int matriceTotal[3][3]){
  int i;
  int j;
  for (i = 0; i < 3; i++) {
    for (j = 0; j < 3; j++) {
      matriceTotal[i][j] = matrice1[i][j]+matrice2[i][j];
    }
  }
}
